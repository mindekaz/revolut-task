package com.accountmanager.tests;

import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.accountmanager.application.interfaces.IRepository;
import com.accountmanager.application.services.AccountService;
import com.accountmanager.domain.entities.Account;
import com.accountmanager.infrastructure.repositories.AccountInMemoryRepo;

public class AccountServiceTests {

	private AccountService accountService;
	private IRepository<Account, String> accountRepo;
	
	@Before
	public void beforeEachTestMethod() {
		accountRepo = new AccountInMemoryRepo();
		accountService = new AccountService(accountRepo);
		
	}
	
	@Test
	public void getMultipleOwnersAccountTest() {
		String accountOwner = "testOwner";

		accountService.createAccount(accountOwner);
		accountService.createAccount(accountOwner);

		assertEquals("incorrect number of accounts",
				2, accountService.getOwnersAccounts(accountOwner).size());
	}
	
	@Test
	public void getZeroOwnersAccountTest() {
		String accountOwner = "testOwner";

		assertTrue("incorrect number of accounts",
				accountService.getOwnersAccounts(accountOwner).isEmpty());
	}
		
	
	@Test
	public void createAccountWithGeneratedNumberTest() {
		String accountOwner = "testOwner";
		accountService.createAccount(accountOwner);
		Optional<Account> account = accountRepo
				.getWhere(x -> x.getOwner().equals(accountOwner)).stream().findFirst();
		assertTrue("Account not or incorrectly persisted",
				account.isPresent());
		assertFalse("Account number not generated",
				account.get().getKey() == null ||
						account.get().getKey().isEmpty());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsOnNonExistingAccount() {
		String accountNumber = "nonExisting";
		accountService.getAccount(accountNumber);
		
	}
	
	

}
