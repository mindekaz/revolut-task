package com.accountmanager.tests;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.accountmanager.application.interfaces.IRepository;
import com.accountmanager.application.services.TransactionService;
import com.accountmanager.domain.entities.Account;
import com.accountmanager.domain.entities.ExternalAccount;
import com.accountmanager.domain.entities.Transaction;
import com.accountmanager.infrastructure.repositories.AccountInMemoryRepo;
import com.accountmanager.infrastructure.repositories.TransactionInMemoryRepo;


public class TransactionServiceTest {

	private TransactionService transactionService;
	private IRepository<Transaction,Integer> transactionRepo;	
	private BigDecimal initBalance = new BigDecimal(500);
	private static AccountInMemoryRepo accountRepo = new AccountInMemoryRepo();
	private static String accountFromNumber;
	private static String accountToNumber;
	
	@BeforeClass
	public static void setupOnce() {
		accountFromNumber = accountRepo.save(createAccount("FromOwner"));
		accountToNumber = accountRepo.save(createAccount("ToOwner"));
		
	}
	
	@Before
	public void setup() {
		transactionRepo = new TransactionInMemoryRepo();
		transactionService = new TransactionService(transactionRepo, accountRepo);
		transactionService.createDepositTransaction(initBalance, accountFromNumber);
		
	}
	
	private static Account createAccount(String owner)
	{
		Account account = new Account(owner);
		return account;
	}
	
	
	@Test
	public void addDepositTransactionTest() {
		
		BigDecimal amount = new BigDecimal(500);
		
		transactionService.createDepositTransaction(amount, accountToNumber);
		assertFalse("Transaction not or incorrectly persisted",
				transactionRepo.getWhere(x ->
					x.getReceiver().getKey().equals(accountToNumber) &&
					x.getAmount().equals(amount) &&
					x.getSender().equals(ExternalAccount.getExternalAccount()))
				.isEmpty());
		assertEquals("Balance incorrect",
				amount, transactionService.getBalance(accountToNumber));
	}
	
	
	@Test
	public void addWithdrawalTransactionTest() {
		BigDecimal amount = new BigDecimal(500);
		Boolean success = transactionService.tryCreateWithrawalTransaction(amount, accountFromNumber);
		assertTrue("Balance check failed", success);
		assertFalse("Transaction not or incorrectly persisted",
				transactionRepo.getWhere(x ->
					x.getReceiver().equals(ExternalAccount.getExternalAccount()) &&
					x.getAmount().equals(amount) &&
					x.getSender().getKey().equals(accountFromNumber))
				.isEmpty());
		
		assertEquals("Balance incorrect",
				BigDecimal.ZERO, transactionService.getBalance(accountFromNumber));
	}
	
	
	@Test
	public void addInternalTransactionTest() {
		BigDecimal amount = new BigDecimal(500);
		
		Boolean success = transactionService.tryCreateInternalTransaction(amount, accountFromNumber, accountToNumber);
		
		assertTrue("Balance check failed", success);
		assertFalse("Transaction not or incorrectly persisted",
				transactionRepo.getWhere(x ->
					x.getReceiver().getKey().equals(accountToNumber) &&
					x.getAmount().equals(amount) &&
					x.getSender().getKey().equals(accountFromNumber))
				.isEmpty());
		
		assertEquals("Balance incorrect",
				BigDecimal.ZERO, transactionService.getBalance(accountFromNumber));
		assertEquals("Balance incorrect",
				amount, transactionService.getBalance(accountToNumber));
	}
	
	@Test
	public void throwsOnTooMuchMoneyTransfer() {
		BigDecimal amount = new BigDecimal(500);
		
		Boolean success = transactionService.tryCreateInternalTransaction(amount.add(BigDecimal.ONE),
				accountFromNumber, accountToNumber);
		assertFalse("Balance check failed to block transaction which is too big", success);
		
		BigDecimal receiversBalance = transactionService.getBalance(accountToNumber);
		BigDecimal sendersBalance = transactionService.getBalance(accountFromNumber);
		assertEquals("Balance incorrect",
				amount, sendersBalance);
		assertEquals("Balance incorrect",
				BigDecimal.ZERO, receiversBalance);
	}
	
	@Test
	public void throwsOnWithdrawTooMuchMoney() {
		BigDecimal amount = new BigDecimal(500);
		
		Boolean success = transactionService.tryCreateWithrawalTransaction(amount.add(BigDecimal.ONE), accountFromNumber);
		assertFalse("Balance check failed to block transaction which is too big", success);

		assertEquals("Balance incorrect",
				amount, transactionService.getBalance(accountFromNumber));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsOnNonExistingAccBalance() {
		transactionService.getBalance("nonExistingAcc");	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsOnNonExistingAccTransaction() {
		BigDecimal transferAmount = BigDecimal.ONE;
		
		transactionService.createDepositTransaction(transferAmount, "nonExistingAcc");	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsOnIncorrectMoneyAmount() {
		BigDecimal transferAmount = BigDecimal.ZERO;
			
		transactionService.tryCreateInternalTransaction(transferAmount, accountFromNumber, accountToNumber);	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsOnSenderAndReceiverIsTheSame() {
		BigDecimal transferAmount = new BigDecimal(10);
			
		transactionService.tryCreateInternalTransaction(transferAmount, accountFromNumber, accountFromNumber);	
	}
	
	@Test
	public void smallAmountsTest() {
		BigDecimal amount1 = new BigDecimal(0.01);
		BigDecimal amount2 = new BigDecimal(0.02);
		BigDecimal amount3 = new BigDecimal(0.03);
		
		transactionService.createDepositTransaction(amount1,  accountToNumber);
		transactionService.createDepositTransaction(amount2,  accountToNumber);
		transactionService.createDepositTransaction(amount3,  accountToNumber);
				
		BigDecimal receiversBalance = transactionService.getBalance(accountToNumber);
		BigDecimal transferedAmount = amount1.add(amount2).add(amount3);
		
		assertEquals("Balance incorrect", transferedAmount, receiversBalance);
	}
	
	
	@Test
	public void largeAmountsTest() {
		BigDecimal amount1 = new BigDecimal(1000000000);
		BigDecimal amount2 = new BigDecimal(300000000);
		BigDecimal amount3 = new BigDecimal(5000000000.0);
		
		transactionService.createDepositTransaction(amount1,  accountToNumber);
		transactionService.createDepositTransaction(amount2,  accountToNumber);
		transactionService.createDepositTransaction(amount3,  accountToNumber);
				
		BigDecimal receiversBalance = transactionService.getBalance(accountToNumber);
		BigDecimal transferedAmount = amount1.add(amount2).add(amount3);
		
		assertEquals("Balance incorrect", transferedAmount, receiversBalance);
	}
	
}
