package com.accountmanager.domain.entities;

import java.util.Objects;

public abstract class Entity<TKey> {
	private TKey key;
	
	public Entity()
	{
		
	}
	
	public Entity(TKey key)
	{
		setKey(key);
	}
	

	
	public TKey getKey()
	{
		return key;
	}
	
	public void setKey(TKey key)
	{
		this.key = key;
	}
	

	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity<TKey> other = (Entity<TKey>) o;
        return Objects.equals(key, other.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

}
