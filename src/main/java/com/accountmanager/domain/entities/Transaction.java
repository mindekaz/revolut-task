package com.accountmanager.domain.entities;

import java.math.BigDecimal;

public class Transaction extends Entity<Integer> {
	private final BigDecimal amount;
	private final Account sender;
	private final Account receiver;
	
	public BigDecimal getAmount() {
		return amount;
	}
	public Account getSender() {
		return sender;
	}
	public Account getReceiver() {
		return receiver;
	}
	
	public Transaction(BigDecimal amount, Account sender, Account receiver) {
		
		super();
		
		if (amount.compareTo(BigDecimal.ZERO) != 1)
			throw new IllegalArgumentException("Non positive trnasaction amount not allowed!");
		
		if (sender == receiver)
			throw new IllegalArgumentException("Sender and receiver cannot be the same account!");
		
		this.amount = amount;
		this.sender = sender;
		this.receiver = receiver;
	}
	

	
}
