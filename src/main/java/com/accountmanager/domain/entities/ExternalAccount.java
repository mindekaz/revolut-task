package com.accountmanager.domain.entities;

public class ExternalAccount extends Account {

	
	private static ExternalAccount externalAccount = new ExternalAccount();
	
	private ExternalAccount() {
		super("");
		setKey("ACC-EXTERNAL");
	}
	
	public static ExternalAccount getExternalAccount()
	{
		return externalAccount;
	}
	
}
