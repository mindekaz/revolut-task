package com.accountmanager.domain.entities;
public class Account extends Entity<String> {

	private final String owner;

	public Account(String owner) 
	{
		super();
		if (owner == null)
			throw new IllegalArgumentException("owner not set");
		this.owner = owner;
	}
	
	public String getOwner() {
		return this.owner;
	}


	
	
}
