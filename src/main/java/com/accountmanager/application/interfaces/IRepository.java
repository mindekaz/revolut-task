package com.accountmanager.application.interfaces;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.accountmanager.domain.entities.Entity;

public interface IRepository<T extends Entity<TKey>, TKey> {
	T get(TKey key);
	Collection<T> getWhere(Predicate<T> predicate);
	TKey save(T entity);
}
