package com.accountmanager.application.services;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.accountmanager.application.interfaces.IRepository;
import com.accountmanager.domain.entities.Account;
import com.accountmanager.domain.entities.ExternalAccount;
import com.accountmanager.domain.entities.Transaction;

public class TransactionService {
	
	private final IRepository<Transaction, Integer> transactionRepo;
	private final IRepository<Account, String> accountRepo;
	private final Map<Account, BigDecimal> balances =  new HashMap<Account, BigDecimal>();
	
	public TransactionService(IRepository<Transaction, Integer> transactionRepo, IRepository<Account, String> accountRepo)
	{
		this.transactionRepo = transactionRepo;
		this.accountRepo = accountRepo;

	}
	

	private void includeTransactionToBalance(Transaction t) 
	{
		Account from = t.getSender();
		Account to = t.getReceiver();
		BigDecimal amount = t.getAmount();
		
		includeIntoBalance(from, amount.multiply(new BigDecimal(-1)));
		includeIntoBalance(to, amount);
		
		
	}
	
	
	private void includeIntoBalance(Account acc, BigDecimal amount)
	{
		if (acc != ExternalAccount.getExternalAccount())
		{
			balances.putIfAbsent(acc, BigDecimal.ZERO);
			BigDecimal newBalance = balances.get(acc).add(amount);
			balances.put(acc, newBalance);
				
		}
	}
	
	
	public void createDepositTransaction(BigDecimal transferAmount, String accountToNumber)
	{
		Account to = keyToAccount(accountToNumber);
		createDepositTransaction(transferAmount, to);
	}
	
	public boolean tryCreateWithrawalTransaction(BigDecimal transferAmount, String accountFromNumber)
	{
		Account from = keyToAccount(accountFromNumber);
		return tryCreateWithrawalTransaction(transferAmount, from);
	}
	
	private Account keyToAccount(String key)
	{
		Account acc = accountRepo.get(key);
		if (acc == null)
			throw new IllegalArgumentException("Account does not exist");
		return acc;
	}
	
	
	public boolean tryCreateInternalTransaction(BigDecimal transferAmount, String accountFromNumber, String accountToNumber)
	{
		Account to =  keyToAccount(accountToNumber);
		Account from =  keyToAccount(accountFromNumber);
		return tryCreateInternalTransaction(transferAmount, from, to);
	}
	
	
	private void createDepositTransaction(BigDecimal transferAmount, Account account)
	{
		Transaction transaction =  new Transaction(transferAmount, ExternalAccount.getExternalAccount(), account);
		processTransaction(transaction);
	}
	
	private boolean tryCreateWithrawalTransaction(BigDecimal transferAmount, Account account)
	{
		return tryCreateReducingTransaction(transferAmount, account, ExternalAccount.getExternalAccount());
	}
	
	
	private boolean tryCreateInternalTransaction(BigDecimal transferAmount, Account from, Account to)
	{
		return tryCreateReducingTransaction(transferAmount, from, to);
	}
	
	private boolean tryCreateReducingTransaction(BigDecimal transferAmount, Account from, Account to)
	{
		Transaction transaction = new Transaction(transferAmount, from, to);
		
		synchronized (from) 
		{
			if(!enoughBalance(transferAmount, from))		
				return false;
			processTransaction(transaction);
		}
		
		
		return true;
	}
	
	private void processTransaction(Transaction transaction)
	{
		transactionRepo.save(transaction);
		includeTransactionToBalance(transaction);
	}


	private boolean enoughBalance(BigDecimal transferAmount, Account acc) {
		BigDecimal currentBalance = getBalance(acc);
		return currentBalance.compareTo(transferAmount) != -1;
	}

	public BigDecimal getBalance(String accNumber) {
		Account acc =  keyToAccount(accNumber);
		return getBalance(acc);
	}

	private BigDecimal getBalance(Account acc) {
		if (!balances.containsKey(acc))
			return BigDecimal.ZERO;
		
		return balances.get(acc);
	}
	

	
	
}
