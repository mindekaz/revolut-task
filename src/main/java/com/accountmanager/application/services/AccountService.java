package com.accountmanager.application.services;

import java.util.Collection;
import java.util.stream.Stream;

import com.accountmanager.application.interfaces.IRepository;
import com.accountmanager.domain.entities.Account;


public class AccountService {
	
	private final IRepository<Account, String> accountRepo;
	public AccountService(IRepository<Account, String> accountRepo)
	{
		this.accountRepo = accountRepo;
	}
	
	
	/*Account management is not the main point of this exercise, so lets make assumption that accounts  
	 * cannot be updated or deleted.
	 */

	public Account createAccount(String owner)
	{
		Account newAcc = new Account(owner);		
		
		this.accountRepo.save(newAcc);
		
		
		return newAcc;
	}



	public Account getAccount(String accountNumber) {
		Account acc = accountRepo.get(accountNumber);
		if (acc == null)
			throw new IllegalArgumentException("Account number does not exist");
		return acc;
	}
	
	public Collection<Account> getOwnersAccounts(String owner) {
		
		/* I do realise this iterates through all the accounts which is not terribly efficient, however, in real world index on the Owner field
		 *  in the database would solve this issue. If we were building in memory cache index could be also added in the code, 
		 *  however, my assumption is that this does not fall within the scope of this exercise.
		 */
		Collection<Account> accs = accountRepo.getWhere(x -> x.getOwner().equals(owner));

		return accs;
	}

	
}
