package com.accountmanager.infrastructure.repositories;

import com.accountmanager.domain.entities.Account;

public class AccountInMemoryRepo extends com.accountmanager.infrastructure.repositories.InMemoryRepository<Account, String> {
	
	@Override
	protected String generateNewKey() {
		return "ACC-" + java.util.UUID.randomUUID();
	}

}
