package com.accountmanager.infrastructure.repositories;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.accountmanager.application.interfaces.IRepository;
import com.accountmanager.domain.entities.Entity;


public abstract class InMemoryRepository<T extends Entity<TKey>, TKey> implements IRepository<T, TKey> {

	private Map<TKey, T> data = new HashMap<TKey, T>();

	@Override
	public T get(TKey key) {
		synchronized (data) {
			return data.get(key);
		}
	}

	@Override
	public Collection<T> getWhere(Predicate<T> predicate) {
		synchronized (data) {
			return data.values().stream()
					.filter(predicate).collect(Collectors.toList());
		}
	}

	@Override
	public TKey save(T entity) {
		if (entity.getKey() == null)
		{
			TKey key = generateNewKey();
			entity.setKey(key);
		}
		synchronized (data) {
			data.put(entity.getKey(), entity);
		}
		return entity.getKey();
	}

	protected abstract TKey generateNewKey() ;
		
	

}
