package com.accountmanager.infrastructure.repositories;

import com.accountmanager.domain.entities.Transaction;

import java.util.concurrent.atomic.AtomicInteger;

public class TransactionInMemoryRepo extends InMemoryRepository<Transaction, Integer>{

	private AtomicInteger lastId = new AtomicInteger(0);
	@Override
	protected Integer generateNewKey() {
		return lastId.incrementAndGet();
	}

}
