package com.accountmanager.presentation;
import static spark.Spark.*;

import com.accountmanager.application.interfaces.IRepository;
import com.accountmanager.application.services.AccountService;
import com.accountmanager.application.services.TransactionService;
import com.accountmanager.domain.entities.Account;
import com.accountmanager.domain.entities.Transaction;
import com.accountmanager.infrastructure.repositories.AccountInMemoryRepo;
import com.accountmanager.infrastructure.repositories.TransactionInMemoryRepo;
import com.accountmanager.presentation.controllers.AccountController;
import com.accountmanager.presentation.controllers.TransactionController;

public class Main {
	public static void main(String[] args) {
		
		//In real life I would replace this with IOT container
        IRepository<Account, String> accountRepo = new AccountInMemoryRepo();
        IRepository<Transaction, Integer> transactionRepo = new TransactionInMemoryRepo();
        
        AccountService accountService = new AccountService(accountRepo);
        TransactionService transactionService = new TransactionService(transactionRepo, accountRepo);

		AccountController accountController = new AccountController(accountService);
		TransactionController transactionController = new TransactionController(transactionService);

        path("/account", () -> {
            post("/", (request, response) ->
					accountController.createAccount(request, response));

            get("/:number", (request, response) ->
					accountController.getAccount(request, response));

            get("/owner/:owner", (request, response) ->
					accountController.getOwnerAccounts(request, response));

            get("/balance/:number", (request, response) ->
					transactionController.getBalance(request, response));
        });
        
        path("/transaction", () -> {
            post("/send", (request, response) ->
					transactionController.send(request, response));
            post("/deposit", (request, response) ->
					transactionController.deposit(request, response));
            post("/withdraw", (request, response) ->
					transactionController.withdraw(request, response));
        });
    }
}
