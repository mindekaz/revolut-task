package com.accountmanager.presentation.controllers;

import com.accountmanager.application.services.TransactionService;
import spark.Request;
import spark.Response;

import java.math.BigDecimal;

public class TransactionController extends Controller
{

    private final TransactionService transactionService;


    public TransactionController(TransactionService transactionService)
    {
        this.transactionService = transactionService;
    }

    public String getBalance(Request request, Response response)
    {
        return Process(request, response,
                r ->  new Object() {
                    public final BigDecimal amount = transactionService.getBalance(r.params(":number"));
                });
    }

    public String send(Request request, Response response)
    {
        return Process(request, response,
                r -> {
                    BigDecimal amount = new BigDecimal(r.queryParams("amount"));
                    String accountFromNumber = r.queryParams("accountFromNumber");
                    String accountToNumber = r.queryParams("accountToNumber");
                    Boolean operationSucceeded = transactionService.tryCreateInternalTransaction(amount,
                            accountFromNumber,
                            accountToNumber);
                    return new Object() {
                        public final boolean success = operationSucceeded;
                    };
                });
    }

    public String withdraw(Request request, Response response)
    {
        return Process(request, response,
                r ->
                {
                    BigDecimal amount = new BigDecimal(r.queryParams("amount"));
                    String accountFromNumber = r.queryParams("accountFromNumber");
                    Boolean operationSucceeded = transactionService.tryCreateWithrawalTransaction(amount, accountFromNumber);

                    return new Object() {
                        public final boolean success = operationSucceeded;
                    };
                });
    }

    public String deposit(Request request, Response response)
    {
        return Process(request, response,
                r ->
                {
                    BigDecimal amount = new BigDecimal(r.queryParams("amount"));
                    String accountToNumber = r.queryParams("accountToNumber");
                    transactionService.createDepositTransaction(amount, accountToNumber);

                    return new Object() {
                        public final boolean success = true;
                    };
                });
    }



}

