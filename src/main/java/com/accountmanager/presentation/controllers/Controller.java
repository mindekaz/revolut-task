package com.accountmanager.presentation.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import spark.Request;
import spark.Response;

import java.util.function.Function;

public abstract class Controller {

    protected ObjectMapper jsonMapper = new ObjectMapper();

    protected <T> String Process(Request request, Response response, Function<Request,T> func)
    {
        try {
            T res = func.apply(request);
            return jsonMapper.writeValueAsString(res);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            response.status(500);
            return "Server error";
        } catch (RuntimeException e)
        {
            e.printStackTrace();
            response.status(500);
            return e.getMessage();
        }
    }
}
