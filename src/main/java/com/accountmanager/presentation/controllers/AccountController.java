package com.accountmanager.presentation.controllers;

import com.accountmanager.application.services.AccountService;
import com.accountmanager.domain.entities.Account;
import spark.Request;
import spark.Response;

public class AccountController extends Controller
{

    private final AccountService accountService;


    public AccountController(AccountService accountService)
    {
        this.accountService = accountService;
    }

    public String createAccount(Request request, Response response)
    {
        return Process(request, response,
                r -> accountService.createAccount(r.queryParams("owner")));
    }

    public String getAccount(Request request, Response response)
    {
        return Process(request, response,
                r -> accountService.getAccount(r.params(":number")));
    }

    public String getOwnerAccounts(Request request, Response response)
    {
        return Process(request, response,
                r -> accountService.getOwnersAccounts(r.params(":owner")));
    }



}
